using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Octatch
{
    public class GameDirector : MonoBehaviour
    {
        // Canvas (debug)
        public GameObject textSeconds;
        public GameObject textCounter;
        public GameObject textProgress;
        public GameObject textMode;
        public GameObject textClassMagic;
        public GameObject textClassAlchemy;

        // Canvas (UI)
        public GameObject buttonCenter;
        public GameObject buttonLeft;
        public GameObject buttonRight;
        public GameObject imageHelp;
        public GameObject imageClear;
        public GameObject buttonTweetClear;
        public GameObject buttonClose;
        public GameObject buttonTweet;
        public GameObject buttonHelp;
        public GameObject buttonAuthor;

        // Screen
        public GameObject screenImage;
        public GameObject screenAnimationParent;
        public GameObject screenAnimation;
        public GameObject screenAnimationPoo;
        public GameObject screenAnimationFlush;

        // Sprite
        public Sprite spriteModeSelectFood;
        public Sprite spriteModeSelectClass;
        public Sprite spriteModeSelectFlush;
        public Sprite spriteFoodSelectCookie;
        public Sprite spriteFoodSelectTea;
        public Sprite spriteClassSelectMagic;
        public Sprite spriteClassSelectAlchemy;
        public Sprite spriteClassMagic;
        public Sprite spriteClassMagicCenter;
        public Sprite spriteClassMagicLeft;
        public Sprite spriteClassMagicRight;
        public Sprite spriteClassMagicCorrect;
        public Sprite spriteClassMagicWrong;
        public Sprite spriteClassAlchemyA;
        public Sprite spriteClassAlchemyB;
        public Sprite spriteClassAlchemyC;

        // AnimatorOverrideController
        public AnimatorOverrideController animatorControllerDormitoryJade;
        public AnimatorOverrideController animatorControllerDormitoryFloyd;
        public AnimatorOverrideController animatorControllerMermaidFloyd;
        public AnimatorOverrideController animatorControllerMermaidJade;
        public AnimatorOverrideController animatorControllerBadFloyd;
        public AnimatorOverrideController animatorControllerFatJade;

        // Screen
        private SpriteRenderer screenImageSpriteRenderer;
        private Animator screenAnimationAnimator;
        private SpriteRenderer screenAnimationSpriteRenderer;
        private Animator screenAnimationPooAnimator;
        private SpriteRenderer screenAnimationPooSpriteRenderer;
        private Animator screenAnimationFlushAnimator;
        private SpriteRenderer screenAnimationFlushSpriteRenderer;

        // JSLIB
        [DllImport("__Internal")]
        private static extern void OpenBlankWindow(string url);

        /// ----------------------------------------------------------------
        /// Main game
        /// ----------------------------------------------------------------

        /// <summary>
        /// Game progress
        /// </summary>
        private enum GameProgress
        {
            Egg,
            Young,
            AdultDormitoryJade,
            AdultDormitoryFloyd,
            AdultMermaidFloyd,
            AdultMermaidJade,
            AdultBadFloyd,
            AdultFatJade,
        }

        /// <summary>
        /// Growth progress of character
        /// </summary>
        private GameProgress gameProgress = GameProgress.Egg;

        /// <summary>
        /// Game mode
        /// </summary>
        private enum GameMode
        {
            None,
            Main,
            ModeSelect,
            FoodSelect,
            FoodCookie,
            FoodTea,
            ClassSelect,
            ClassMagic,
            ClassMagicPlaying,
            ClassAlchemy,
            ClassAlchemyPlaying,
            Flush,
        }

        /// <summary>
        /// Current game mode
        /// </summary>
        private GameMode currentGameMode = GameMode.None;

        /// <summary>
        /// Selected game mode
        /// </summary>
        private GameMode selectedGameMode = GameMode.None;

        /// <summary>
        /// Button type
        /// </summary>
        private enum ButtonType
        {
            None,
            Center,
            Left,
            Right,
        }

        /// <summary>
        /// Pressed button
        /// </summary>
        private ButtonType pressedButton = ButtonType.None;

        /// <summary>
        /// Count of cookie
        /// </summary>
        private int countOfCookie = 0;

        /// <summary>
        /// Count of tea
        /// </summary>
        private int countOfTea = 0;

        /// <summary>
        /// Count of magic mini game success
        /// </summary>
        private int countOfMagic = 0;

        /// <summary>
        /// Count of alchemy mini game success
        /// </summary>
        private int countOfAlchemy = 0;

        /// <summary>
        /// Count of poo
        /// </summary>
        private int countOfPoo = 0;

        /// <summary>
        /// Passed seconds of total
        /// </summary>
        private float totalSeconds = 0.0f;

        /// <summary>
        /// Passed seconds from last button press
        /// </summary>
        private float leaveSeconds = 0.0f;

        /// <summary>
        /// Title seconds
        /// </summary>
        private float titleSeconds = 8.0f;

        /// <summary>
        /// Is clear popup displayed?
        /// </summary>
        private bool isClearPopupDisplayed = false;

        /// ----------------------------------------------------------------
        /// Animation
        /// ----------------------------------------------------------------

        /// <summary>
        /// Animation state (as same as trigger name)
        /// </summary>
        private enum AnimationState
        {
            Egg,
            Idle,
            FoodCookie,
            FoodTea,
            ClassMagic,
            ClassAlchemy,
            Poo,
            Flush,
            Success,
            Failure,
            Evolution,
        }

        /// <summary>
        /// Animation state list what can not press button while animation playing
        /// </summary>
        List<AnimationState> buttonDisabledStateList = new List<AnimationState>
        {
            AnimationState.Egg,
            AnimationState.FoodCookie,
            AnimationState.FoodTea,
            AnimationState.Flush,
            AnimationState.Success,
            AnimationState.Failure,
            AnimationState.Evolution,
        };

        /// ----------------------------------------------------------------
        /// Magic mini game
        /// ----------------------------------------------------------------

        /// <summary>
        /// Magic mini game mode
        /// </summary>
        private enum MagicGameMode
        {
            Ready,
            MakeProbrem,
            WaitAnswer,
        }

        /// <summary>
        /// Magic mini game state
        /// </summary>
        private MagicGameMode magicGameMode = MagicGameMode.MakeProbrem;

        /// <summary>
        /// Magic mini game answer
        /// </summary>
        private ButtonType magicAnswer = ButtonType.None;

        /// <summary>
        /// Magic mini game passed seconds
        /// </summary>
        private float magicSeconds = 0.0f;

        /// <summary>
        /// Magic mini game time limit seconds
        /// </summary>
        private float magicTimeLimitSeconds = 3.0f;

        /// <summary>
        /// Magic mini game clear count
        /// </summary>
        private int magicClearCount = 3;

        /// <summary>
        /// Magic mini game correct count
        /// </summary>
        private int magicCorrectCount = 0;

        /// <summary>
        /// Magic mini game wrong count
        /// </summary>
        private int magicWrongCount = 0;

        /// ----------------------------------------------------------------
        /// Alchemy mini game
        /// ----------------------------------------------------------------

        /// <summary>
        /// Alchemy mini game passed seconds
        /// </summary>
        private float alchemySeconds = 0.0f;

        /// <summary>
        /// Alchemy mini game time limit seconds
        /// </summary>
        private float alchemyTimeLimitSeconds = 10.0f;

        /// <summary>
        /// Alchemy mini game clear count
        /// </summary>
        private int alchemyClearCount = 20;

        /// <summary>
        /// Alchemy mini game hit count
        /// </summary>
        private int alchemyHitCount = 0;

        /// ----------------------------------------------------------------
        /// Start
        /// ----------------------------------------------------------------

        void Start()
        {
            // Fix to 60 fps
            Application.targetFrameRate = 60;

            // Get screen component
            screenImageSpriteRenderer = screenImage.GetComponent<SpriteRenderer>();
            screenAnimationAnimator = screenAnimation.GetComponent<Animator>();
            screenAnimationSpriteRenderer = screenAnimation.GetComponent<SpriteRenderer>();
            screenAnimationPooAnimator = screenAnimationPoo.GetComponent<Animator>();
            screenAnimationPooSpriteRenderer = screenAnimationPoo.GetComponent<SpriteRenderer>();
            screenAnimationFlushAnimator = screenAnimationFlush.GetComponent<Animator>();
            screenAnimationFlushSpriteRenderer = screenAnimationFlush.GetComponent<SpriteRenderer>();

            // Add listener to buttons
            buttonCenter.GetComponent<Button>().onClick.AddListener(ButtonCenterPressed);
            buttonLeft.GetComponent<Button>().onClick.AddListener(ButtonLeftPressed);
            buttonRight.GetComponent<Button>().onClick.AddListener(ButtonRightPressed);
            buttonTweetClear.GetComponent<Button>().onClick.AddListener(ButtonTweetPressed);
            buttonClose.GetComponent<Button>().onClick.AddListener(ButtonClosePressed);
            buttonTweet.GetComponent<Button>().onClick.AddListener(ButtonTweetPressed);
            buttonHelp.GetComponent<Button>().onClick.AddListener(ButtonHelpPressed);
            buttonAuthor.GetComponent<Button>().onClick.AddListener(ButtonAuthorPressed);
        }

        /// ----------------------------------------------------------------
        /// Update
        /// ----------------------------------------------------------------

        void Update()
        {
            // Timer
            totalSeconds += Time.deltaTime;
            leaveSeconds += Time.deltaTime;

            // Game
            UpdateGameProgress();
            UpdatePoo();
            UpdateDebugText();

            if (CanPressButton())
            {               
                if (currentGameMode == GameMode.ClassMagicPlaying)
                {
                    PlayClassMagic();
                }
                else if (currentGameMode == GameMode.ClassAlchemyPlaying)
                {
                    PlayClassAlchemy();
                }
                else
                {
                    WatchMenu();
                }
            }
            else
            {
                ResetPressedButton();
            }
        }

        /// ----------------------------------------------------------------
        /// Methods
        /// ----------------------------------------------------------------

        /// <summary>
        /// When center button pressed
        /// </summary>
        public void ButtonCenterPressed()
        {
            pressedButton = ButtonType.Center;
            leaveSeconds = 0.0f;
            Debug.Log("Center button down");
        }

        /// <summary>
        /// When left button pressed
        /// </summary>
        public void ButtonLeftPressed()
        {
            pressedButton = ButtonType.Left;
            leaveSeconds = 0.0f;
            Debug.Log("Left button down");
        }

        /// <summary>
        /// When right button pressed
        /// </summary>
        public void ButtonRightPressed()
        {
            pressedButton = ButtonType.Right;
            leaveSeconds = 0.0f;
            Debug.Log("Right button down");
        }

        /// <summary>
        /// When tweet button pressed
        /// </summary>
        public void ButtonTweetPressed()
        {
            // Make text
            string text;

            if (gameProgress == GameProgress.Egg || gameProgress == GameProgress.Young)
            {
                text = "おくたっちをプレイ中！";
            }
            else
            {
                string name = "";

                if (gameProgress == GameProgress.AdultDormitoryJade)
                {
                    name = "寮服ジェイド";
                }
                else if (gameProgress == GameProgress.AdultDormitoryFloyd)
                {
                    name = "寮服フロイド";
                }
                else if (gameProgress == GameProgress.AdultMermaidFloyd)
                {
                    name = "人魚フロイド";
                }
                else if (gameProgress == GameProgress.AdultMermaidJade)
                {
                    name = "人魚ジェイド";
                }
                else if (gameProgress == GameProgress.AdultBadFloyd)
                {
                    name = "不良フロイド";
                }
                else if (gameProgress == GameProgress.AdultFatJade)
                {
                    name = "デブジェイド";
                }
                else
                {
                    // PASS
                }

                text = $"稚魚が進化して{name}になった！";
            }

            text = UnityWebRequest.EscapeURL(text);

            // URL, Hashtags
            string url = "https://games.gloxi.net/octatch/";
            string hashtags = UnityWebRequest.EscapeURL("おくたっち");

            // Tweet
            Application.OpenURL($"https://twitter.com/intent/tweet?text={text}&url={url}&hashtags={hashtags}");

            // Hide popup
            HidePopup();
        }

        /// <summary>
        /// When help button pressed
        /// </summary>
        public void ButtonHelpPressed()
        {
            ShowPopupHelp();
        }

        /// <summary>
        /// When close button pressed
        /// </summary>
        public void ButtonClosePressed()
        {
            HidePopup();
        }

        /// <summary>
        /// Show help popup
        /// </summary>
        private void ShowPopupHelp()
        {
            imageHelp.SetActive(true);
            buttonClose.SetActive(true);
            PauseGame();
        }

        /// <summary>
        /// Show clear popup
        /// </summary>
        private void ShowPopupClear()
        {
            imageClear.SetActive(true);
            buttonTweetClear.SetActive(true);
            buttonClose.SetActive(true);
            PauseGame();
        }

        /// <summary>
        /// Hide popup
        /// </summary>
        private void HidePopup()
        {
            imageHelp.SetActive(false);
            imageClear.SetActive(false);
            buttonTweetClear.SetActive(false);
            buttonClose.SetActive(false);
            ResumeGame();
        }

        /// <summary>
        /// Pause game
        /// </summary>
        private void PauseGame()
        {
            Time.timeScale = 0.0f;
        }

        /// <summary>
        /// Resume game
        /// </summary>
        private void ResumeGame()
        {
            Time.timeScale = 1.0f;
        }

        /// <summary>
        /// Reset pressed button
        /// </summary>
        private void ResetPressedButton()
        {
            pressedButton = ButtonType.None;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ButtonAuthorPressed()
        {
            string url = "https://twitter.com/rerakamu";
            Application.OpenURL(url);
        }

        /// <summary>
        /// Can press button?
        /// </summary>
        /// <returns>Return true when can press button</returns>
        private bool CanPressButton()
        {
            bool canPressDown = true;

            // AnimationAnimator
            AnimatorStateInfo state = screenAnimationAnimator.GetCurrentAnimatorStateInfo(0);


            foreach (AnimationState disableState in buttonDisabledStateList)
            {
                // If IsName() return true, canPressDown is turned false
                canPressDown = canPressDown && (state.IsName(disableState.ToString()) == false);
            }

            // AnimationFlushAnimator
            AnimatorStateInfo stateFlush = screenAnimationFlushAnimator.GetCurrentAnimatorStateInfo(0);
            canPressDown = canPressDown && (stateFlush.IsName("Curtain") == false);

            return canPressDown;
        }

        /// <summary>
        /// Watch counter and update gameProgress
        /// </summary>
        private void UpdateGameProgress()
        {
            if (gameProgress == GameProgress.Egg)
            {
                // Egg
                if (totalSeconds >= titleSeconds)
                {
                    currentGameMode = GameMode.Main;
                    leaveSeconds = 0.0f;

                    gameProgress = GameProgress.Young;
                    ShowAnimation(AnimationState.Idle);
                }
            }
            else if (gameProgress == GameProgress.Young)
            {
                // Young
                if (countOfPoo == 0)
                {
                    // When have no poo
                    if (countOfTea >= 2 && countOfMagic >= 2)
                    {
                        gameProgress = GameProgress.AdultDormitoryJade;
                        SetAnimatorController(animatorControllerDormitoryJade);
                        ShowAnimation(AnimationState.Evolution);
                    }
                    else if (countOfCookie >= 2 && countOfAlchemy >= 2)
                    {
                        gameProgress = GameProgress.AdultDormitoryFloyd;
                        SetAnimatorController(animatorControllerDormitoryFloyd);
                        ShowAnimation(AnimationState.Evolution);
                    }
                    else if (countOfCookie >= 1 && countOfTea >= 1 && countOfAlchemy >= 1)
                    {
                        gameProgress = GameProgress.AdultMermaidFloyd;
                        SetAnimatorController(animatorControllerMermaidFloyd);
                        ShowAnimation(AnimationState.Evolution);
                    }
                    else if (countOfTea >= 1 && countOfMagic >= 1 && countOfAlchemy >= 1)
                    {
                        gameProgress = GameProgress.AdultMermaidJade;
                        SetAnimatorController(animatorControllerMermaidJade);
                        ShowAnimation(AnimationState.Evolution);
                    }
                    else
                    {
                        // PASS
                    }
                }
                else if (countOfPoo == 5 && (countOfCookie >= 1 || countOfTea >= 1) && CanPressButton())
                {
                    // When have 5 poo

                    // Reset poo
                    countOfPoo = 0;
                    leaveSeconds = 0.0f;
                    screenAnimationPooAnimator.SetInteger("Count", 0);

                    Vector3 parentPosition = screenAnimationParent.transform.position;
                    parentPosition.x = 0.0f;
                    screenAnimationParent.transform.position = parentPosition;

                    if (countOfCookie > countOfTea)
                    {
                        gameProgress = GameProgress.AdultBadFloyd;
                        SetAnimatorController(animatorControllerBadFloyd);
                    }
                    else if (countOfCookie < countOfTea)
                    {
                        gameProgress = GameProgress.AdultFatJade;
                        SetAnimatorController(animatorControllerFatJade);
                    }
                    else
                    {
                        int random = Random.Range(0, 2); // 0 or 1

                        if (random == 0)
                        {
                            gameProgress = GameProgress.AdultBadFloyd;
                            SetAnimatorController(animatorControllerBadFloyd);
                        }
                        else
                        {
                            gameProgress = GameProgress.AdultFatJade;
                            SetAnimatorController(animatorControllerFatJade);
                        }
                    }

                    ShowAnimation(AnimationState.Evolution);
                }
                else
                {
                    // When have 1-4 poo: PASS
                }
            }
            else
            {
                // Adult
                if (isClearPopupDisplayed == false && CanPressButton())
                {
                    isClearPopupDisplayed = true;
                    ShowPopupClear();
                }
            }
        }

        /// <summary>
        /// Watch button input and update currentGameMode/selectedGameMode
        /// </summary>
        private void WatchMenu()
        {
            if (currentGameMode == GameMode.Main)
            {
                // --------------------------------
                //  Main
                // --------------------------------

                if (pressedButton == ButtonType.Center)
                {
                    // Center: Main => ModeSelect (FoodSelect)
                    currentGameMode = GameMode.ModeSelect;
                    selectedGameMode = GameMode.FoodSelect;
                    ShowSprite(spriteModeSelectFood);
                }
                else
                {
                    // Left, Right: PASS
                }
            }
            else if (currentGameMode == GameMode.ModeSelect)
            {
                // --------------------------------
                //  ModeSelect
                // --------------------------------

                if (pressedButton == ButtonType.Center)
                {
                    // Center: ModeSelect => FoodSelect/ClassSelect/Flush
                    currentGameMode = selectedGameMode;

                    if (selectedGameMode == GameMode.FoodSelect)
                    {
                        selectedGameMode = GameMode.FoodCookie;
                        ShowSprite(spriteFoodSelectCookie);
                    }
                    else if (selectedGameMode == GameMode.ClassSelect)
                    {
                        selectedGameMode = GameMode.ClassMagic;
                        ShowSprite(spriteClassSelectMagic);
                    }
                    else if (selectedGameMode == GameMode.Flush)
                    {
                        selectedGameMode = GameMode.None;
                    }
                    else
                    {
                        // PASS
                    }
                }
                else if (pressedButton == ButtonType.Left)
                {
                    // Left
                    if (selectedGameMode == GameMode.FoodSelect)
                    {
                        // FoodSelect -> ClassSelect
                        selectedGameMode = GameMode.ClassSelect;
                        ShowSprite(spriteModeSelectClass);
                    }
                    else if (selectedGameMode == GameMode.ClassSelect)
                    {
                        // ClassSelect -> Flush
                        selectedGameMode = GameMode.Flush;
                        ShowSprite(spriteModeSelectFlush);
                    }
                    else if (selectedGameMode == GameMode.Flush)
                    {
                        // Flush -> FoodSelect
                        selectedGameMode = GameMode.FoodSelect;
                        ShowSprite(spriteModeSelectFood);
                    }
                    else
                    {
                        // PASS
                    }
                }
                else if (pressedButton == ButtonType.Right)
                {
                    // Right: ModeSelect => Main (None)
                    currentGameMode = GameMode.Main;
                    selectedGameMode = GameMode.None;
                    ShowAnimation(AnimationState.Idle);
                }
                else
                {
                    // PASS
                }
            }
            else if (currentGameMode == GameMode.FoodSelect)
            {
                // --------------------------------
                //  FoodSelect
                // --------------------------------

                if (pressedButton == ButtonType.Center)
                {
                    // Center
                    if (selectedGameMode == GameMode.FoodCookie)
                    {
                        countOfCookie += 1;
                        ShowAnimation(AnimationState.FoodCookie);
                    }
                    else if (selectedGameMode == GameMode.FoodTea)
                    {
                        countOfTea += 1;
                        ShowAnimation(AnimationState.FoodTea);                        
                    }
                    else
                    {
                        // PASS
                    }

                    // FoodSelect => FoodCookie/FoodTea => Main (None)
                    currentGameMode = GameMode.Main;
                    selectedGameMode = GameMode.None;
                }
                else if (pressedButton == ButtonType.Left)
                {
                    // Left
                    if (selectedGameMode == GameMode.FoodTea)
                    {
                        // FoodTea -> FoodCookie
                        selectedGameMode = GameMode.FoodCookie;
                        ShowSprite(spriteFoodSelectCookie);
                    }
                    else if (selectedGameMode == GameMode.FoodCookie)
                    {
                        // FoodCookie -> FoodTea
                        selectedGameMode = GameMode.FoodTea;
                        ShowSprite(spriteFoodSelectTea);
                    }
                    else
                    {
                        // PASS
                    }
                }
                else if (pressedButton == ButtonType.Right)
                {
                    // Right: FoodSelect => ModeSelect (FoodSelect)
                    currentGameMode = GameMode.ModeSelect;
                    selectedGameMode = GameMode.FoodSelect;
                    ShowSprite(spriteModeSelectFood);
                }
                else
                {
                    // PASS
                }
            }
            else if (currentGameMode == GameMode.ClassSelect)
            {
                // --------------------------------
                //  ClassSelect
                // --------------------------------

                if (pressedButton == ButtonType.Center)
                {
                    // Center: ClassSelect => ClassMagic/ClassAlchemy (None)
                    currentGameMode = selectedGameMode;
                    selectedGameMode = GameMode.None;
                }
                else if (pressedButton == ButtonType.Left)
                {
                    // Left
                    if (selectedGameMode == GameMode.ClassMagic)
                    {
                        // ClassMagic -> ClassAlchemy
                        selectedGameMode = GameMode.ClassAlchemy;
                        ShowSprite(spriteClassSelectAlchemy);
                    }
                    else if (selectedGameMode == GameMode.ClassAlchemy)
                    {
                        // ClassAlchemy -> ClassMagic
                        selectedGameMode = GameMode.ClassMagic;
                        ShowSprite(spriteClassSelectMagic);
                    }
                    else
                    {
                        // PASS
                    }
                }
                else if (pressedButton == ButtonType.Right)
                {
                    // Right: FoodSelect => ModeSelect (FoodSelect)
                    currentGameMode = GameMode.ModeSelect;
                    selectedGameMode = GameMode.FoodSelect;
                    ShowSprite(spriteModeSelectFood);
                }
                else
                {
                    // PASS
                }
            }
            else if (currentGameMode == GameMode.ClassMagic)
            {
                // --------------------------------
                //  ClassMagic
                // --------------------------------

                currentGameMode = GameMode.ClassMagicPlaying;
                magicGameMode = MagicGameMode.Ready;
                magicSeconds = 0.0f;
                magicCorrectCount = 0;
                magicWrongCount = 0;
                ShowAnimation(AnimationState.ClassMagic);
                ShowSprite(spriteClassMagic);
            }
            else if (currentGameMode == GameMode.ClassAlchemy)
            {
                // --------------------------------
                //  ClassAlchemy
                // --------------------------------

                currentGameMode = GameMode.ClassAlchemyPlaying;
                alchemySeconds = 0.0f;
                alchemyHitCount = 0;
                ShowAnimation(AnimationState.ClassAlchemy);
                HideScreenAnimationPoo();
            }
            else if (currentGameMode == GameMode.Flush)
            {
                // --------------------------------
                //  Flush
                // --------------------------------

                // Flush => Main
                currentGameMode = GameMode.Main;
                SetPoo(0);
                ShowAnimation(AnimationState.Flush);
            }
            else
            {
                // PASS
            }

            // Reset pressed button
            ResetPressedButton();
        }


        /// <summary>
        /// Show screen image
        /// </summary>
        private void ShowScreenImage()
        {
            screenImageSpriteRenderer.enabled = true;
            screenAnimationSpriteRenderer.enabled = false;
            screenAnimationPooSpriteRenderer.enabled = false;
        }

        /// <summary>
        /// Show screen animation
        /// </summary>
        private void ShowScreenAnimation()
        {
            screenImageSpriteRenderer.enabled = false;
            screenAnimationSpriteRenderer.enabled = true;
            screenAnimationPooSpriteRenderer.enabled = true;
        }

        /// <summary>
        /// Show screen animation poo
        /// </summary>
        private void ShowScreenAnimationPoo()
        {
            screenAnimationPooSpriteRenderer.enabled = true;
            StartCoroutine(MoveScreenAnimation(countOfPoo, true));
        }

        /// <summary>
        /// Hide screen animation poo
        /// </summary>
        private void HideScreenAnimationPoo()
        {
            screenAnimationPooSpriteRenderer.enabled = false;
            StartCoroutine(MoveScreenAnimation(0));
        }

        /// <summary>
        /// Show sprite
        /// </summary>
        /// <param name="sprite">Sprite</param>
        private void ShowSprite(Sprite sprite)
        {
            ShowScreenImage();
            screenImageSpriteRenderer.sprite = sprite;
        }

        /// <summary>
        /// Show animation
        /// </summary>
        /// <param name="state">Animation state</param>
        private void ShowAnimation(AnimationState state)
        {
            ShowScreenAnimation();
            screenAnimationAnimator.SetTrigger(state.ToString());
        }

        /// <summary>
        /// Set animator controller
        /// </summary>
        /// <param name="animatorController">AnimatorOverrideController</param>
        private void SetAnimatorController(AnimatorOverrideController animatorController)
        {
            screenAnimationAnimator.runtimeAnimatorController = animatorController as RuntimeAnimatorController;
        }

        /// <summary>
        /// Play magic mini game
        /// </summary>
        private void PlayClassMagic()
        {
            // Playing
            // Ready -> Make probrem -> Wait answer -> Ready -> ...
            magicSeconds += Time.deltaTime;

            if (magicGameMode == MagicGameMode.Ready)
            {
                // Ready
                if (magicSeconds >= 1.0f)
                {
                    magicGameMode = MagicGameMode.MakeProbrem;
                }
            }
            else if (magicGameMode == MagicGameMode.MakeProbrem)
            {
                // Make probrem
                magicAnswer = GetRandomMagicAnswer();

                if (magicAnswer == ButtonType.Center)
                {
                    ShowSprite(spriteClassMagicCenter);
                }
                else if (magicAnswer == ButtonType.Left)
                {
                    ShowSprite(spriteClassMagicLeft);
                }
                else if (magicAnswer == ButtonType.Right)
                {
                    ShowSprite(spriteClassMagicRight);
                }
                else
                {
                    // PASS
                }

                magicGameMode = MagicGameMode.WaitAnswer;
                magicSeconds = 0.0f;
            }
            else if (magicGameMode == MagicGameMode.WaitAnswer)
            {
                // Wait answer
                if (pressedButton != ButtonType.None)
                {
                    if (pressedButton == magicAnswer)
                    {
                        // Correct
                        magicCorrectCount += 1;
                        ShowSprite(spriteClassMagicCorrect);
                    }
                    else
                    {
                        // Wrong (Button)
                        magicWrongCount += 1;
                        ShowSprite(spriteClassMagicWrong);
                    }

                    ResetPressedButton();
                    magicGameMode = MagicGameMode.Ready;
                    magicSeconds = 0.0f;
                }

                if (magicSeconds >= magicTimeLimitSeconds)
                {
                    // Wrong (Time limit)
                    magicWrongCount += 1;
                    ShowSprite(spriteClassMagicWrong);
                    magicGameMode = MagicGameMode.Ready;
                    magicSeconds = 0.0f;
                }
            }
            else
            {
                // PASS
            }

            // Finish
            if (magicGameMode == MagicGameMode.MakeProbrem && magicCorrectCount + magicWrongCount == magicClearCount)
            {
                currentGameMode = GameMode.Main;

                if (magicCorrectCount == magicClearCount)
                {
                    // Success
                    countOfMagic += 1;
                    ShowAnimation(AnimationState.Success);
                }
                else
                {
                    // Failure
                    ShowAnimation(AnimationState.Failure);
                }
            }
        }

        /// <summary>
        /// Get random magic answer (Center/Left/Right)
        /// </summary>
        /// <returns>Random magic answer</returns>
        private ButtonType GetRandomMagicAnswer()
        {
            int random = Random.Range(1, 4); // 1 or 2 or 3

            if (random == 1)
            {
                return ButtonType.Center;
            }
            else if (random == 2)
            {
                return ButtonType.Left;
            }
            else if (random == 3)
            {
                return ButtonType.Right;
            }
            else
            {
                // ERROR
                return ButtonType.None;
            }
        }

        /// <summary>
        /// Play alchemy mini game
        /// </summary>
        private void PlayClassAlchemy()
        {
            // Playing
            alchemySeconds += Time.deltaTime;

            if (pressedButton != ButtonType.None)
            {
                // Button down
                alchemyHitCount += 1;
                ResetPressedButton();
            }

            // Finish
            if (alchemySeconds >= alchemyTimeLimitSeconds)
            {
                currentGameMode = GameMode.Main;

                if (alchemyHitCount >= alchemyClearCount)
                {
                    // Success
                    countOfAlchemy += 1;
                    ShowAnimation(AnimationState.Success);
                }
                else
                {
                    // Failure
                    ShowAnimation(AnimationState.Failure);
                }

                ShowScreenAnimationPoo();
            }
        }

        /// <summary>
        /// Update poo counts
        /// </summary>
        private void UpdatePoo()
        {
            if (currentGameMode == GameMode.Main)
            {
                if (countOfPoo == 0 && leaveSeconds >= 20.0f)
                {
                    // 0 -> 1
                    SetPoo(1);
                }
                else if (countOfPoo == 1 && leaveSeconds >= 35.0f)
                {
                    // 1 -> 2
                    SetPoo(2);
                }
                else if (countOfPoo == 2 && leaveSeconds >= 50.0f)
                {
                    // 2 -> 3
                    SetPoo(3);
                }
                else if (countOfPoo == 3 && leaveSeconds >= 65.0f)
                {
                    // 3 -> 4
                    SetPoo(4);
                }
                else if (countOfPoo == 4 && leaveSeconds >= 80.0f)
                {
                    // 4 -> 5
                    SetPoo(5);
                }
                else
                {
                    // GTE 5: PASS
                }
            }
            else
            {
                leaveSeconds = 0.0f;
            }
        }

        /// <summary>
        /// Set count of poo
        /// </summary>
        /// <param name="count">Count of poo</param>
        private void SetPoo(int count)
        {
            countOfPoo = count;

            // Animation
            if (count != 0)
            {
                screenAnimationAnimator.SetTrigger("Poo");
                screenAnimationFlushAnimator.SetTrigger("Curtain");
            }

            StartCoroutine(PlayPooAnimation(count));

            // Move ScreenAnimation right
            StartCoroutine(MoveScreenAnimation(count));
        }

        /// <summary>
        /// Play poo animation
        /// </summary>
        /// <param name="count">Count of poo</param>
        /// <returns></returns>
        private IEnumerator PlayPooAnimation(int count)
        {
            // 1s: Character shaking
            // 1s: Curtain falling
            // 1s: Curtain covering
            float wait = 2.5f;

            if (count == 0)
            {
                // Flush
                // 1s: Flush passing center
                wait = 1.0f;
            }

            yield return new WaitForSeconds(wait);

            screenAnimationPooAnimator.SetInteger("Count", count);
        }

        /// <summary>
        /// Move ScreenAnimation
        /// </summary>
        /// <param name="count">Count of poo</param>
        /// <returns></returns>
        private IEnumerator MoveScreenAnimation(int count, bool noWait = false)
        {
            // 1s: Character shaking
            // 1s: Curtain falling
            // 1s: Curtain covering
            float wait = 2.5f;

            if (count == 0 || noWait)
            {
                // Flush
                wait = 0.0f;
            }

            yield return new WaitForSeconds(wait);

            Vector3 parentPosition = screenAnimationParent.transform.position;

            if (count == 0)
            {
                // 0
                parentPosition.x = 0.0f;
            }
            else if (count == 1 || count == 2)
            {
                // 1, 2
                parentPosition.x = 0.75f;
            }
            else if (count == 3 || count == 4)
            {
                // 3, 4
                parentPosition.x = 1.5f;
            }
            else if (count == 5)
            {
                // 5
                parentPosition.x = 2.25f;
            }
            else
            {
                // PASS
            }

            screenAnimationParent.transform.position = parentPosition;
        }

        /// <summary>
        /// Update debug text
        /// </summary>
        private void UpdateDebugText()
        {
            textSeconds.GetComponent<Text>().text = $"Total: {totalSeconds:F2} s, Leave: {leaveSeconds:F2} s";
            textCounter.GetComponent<Text>().text = $"Cookie: {countOfCookie}, Tea: {countOfTea}, Magic: {countOfMagic}, Alchemy: {countOfAlchemy}, Poo: {countOfPoo}";
            textProgress.GetComponent<Text>().text = $"Progress: {gameProgress}";
            textMode.GetComponent<Text>().text = $"Current: {currentGameMode}, Selected: {selectedGameMode}, Button: {CanPressButton()}";
            textClassMagic.GetComponent<Text>().text = $"<Magic> Limit: {magicSeconds:F2} s, Answer: {magicAnswer}, Correct: {magicCorrectCount}";
            textClassAlchemy.GetComponent<Text>().text = $"<Alchemy> Limit: {alchemySeconds:F2} s, Hits: {alchemyHitCount}";
        }
    }
}
